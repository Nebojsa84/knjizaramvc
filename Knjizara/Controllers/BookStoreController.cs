﻿using Knjizara.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Knjizara.ViewModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Knjizara.Repository.Interfaces;
using Knjizara.Repository;

namespace Knjizara.Controllers
{
    public class BookStoreController : Controller
    {
        private IBookRepository BookRepository = new BookRepository();
        private IGenreRepository GenreRepository = new GenreRepository();

        // GET: BookStore
        public ActionResult Index()
        {
            return View(GenreRepository.GetAll());
        }


        public ActionResult Create(Book b, int genre)
        {

            BookRepository.Create(b, genre);
            return RedirectToAction("Read");
        }

        public ActionResult Read()
        {
            return View(BookRepository.GetAll());
        }

        public ActionResult Update(int id)
        {
            BookGenreViewModel retVal = new BookGenreViewModel();

            retVal.Book = BookRepository.GetById(id);
            retVal.Genres = GenreRepository.GetAll().ToList();
            retVal.SelectedGenre = BookRepository.GetById(id).Genre.Id; // Izabrani id zanra

            return View(retVal);
        }


        [HttpPost]
        public ActionResult Update(BookGenreViewModel b)
        {
            BookRepository.Update(b);
            return RedirectToAction("Read");
        }

        public ActionResult ShowDeleted()
        {                                  
            return View(BookRepository.GetDeleted().ToList());
        }

        public ActionResult Delete(int id)
        {
            BookRepository.Delete(id);

            return RedirectToAction("Read");
        }

        public ActionResult Search()
        {
            return View();
        }

        public ActionResult SearchResult(string name, string criteria)
        {          
            return View(BookRepository.FindBook(name,criteria).ToList());
        }

    }
}
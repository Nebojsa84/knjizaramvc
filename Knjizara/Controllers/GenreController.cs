﻿using Knjizara.Models;
using Knjizara.Repository;
using Knjizara.Repository.Interfaces;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.Mvc;

namespace Knjizara.Controllers
{
    public class GenreController : Controller
    {
        private IGenreRepository GenreRepository = new GenreRepository();
        // GET: Genre
        public ActionResult Index()
        {

            return View(GenreRepository.GetAll());
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(string name)
        {
            GenreRepository.Create(name);
            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
           GenreRepository.Delete(id);                     
           return RedirectToAction("Index");
        }
    }
}
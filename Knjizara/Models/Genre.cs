﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Knjizara.Models
{
    public class Genre
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Deleted { get; set; }


        public Genre(int id, string name, bool deleted)
        {
            this.Id = id;
            this.Name = name;
            this.Deleted = deleted;
        }
        public Genre()
        {

        }

        public Genre(string name)
        {
            this.Id = Project.Genres.Count;
            this.Name = name;
            this.Deleted = false;
        }
    }
}
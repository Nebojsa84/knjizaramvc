﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Knjizara.Models
{
    public class Book
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public Genre Genre { get; set; }
        public List <Chapter> Chapters { get; set; }
        public bool Deleted { get; set; }

        public Book(string name,double price,Genre genre,List<Chapter> chapters, bool deleted)
        {           
            this.Name = name;
            this.Price = price;
            this.Genre = genre;
            this.Chapters = chapters;         
        }

        public Book(int id,string name, double price, Genre genre,List<Chapter> chapters, bool deleted)
        {
            this.Id = id;
            this.Name = name;
            this.Price = price;
            this.Genre = genre;
            this.Chapters = chapters;
            this.Deleted = deleted;
        }

        public Book()
        {
            this.Genre = new Genre();

            this.Chapters = new List<Chapter>();

        }
    }
}
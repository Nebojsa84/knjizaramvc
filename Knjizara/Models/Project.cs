﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Knjizara.Models
{
    public class Project
    {
        public static List<Chapter> ChaptersB1 = new List<Chapter>()
        {
            new Chapter(0, "Chapter 1"),
            new Chapter(1, "Chapter 2"),
            new Chapter(2, "Chapter 3")
        };

        public static List<Chapter> ChaptersB2 = new List<Chapter>()
        {
            new Chapter(0, "The Texan"),
            new Chapter(1, "Clevinger"),
            new Chapter(2, "Havermeyer"),
        };

        public static List<Chapter> ChaptersB3= new List<Chapter>()
        {
            new Chapter(0, "Part 1"),
            new Chapter(1, "Part 2"),
            new Chapter(2, "Part 3"),
        };

        public static List<Chapter> ChaptersB4 = new List<Chapter>()
        {
            new Chapter(0, "Intro"),
            new Chapter(1, "Plot"),
            new Chapter(2, "Ending"),
        };

        public static List<Genre> Genres { get; set; } = new List<Genre>()
        {
            new Genre(0,"Science",false),
            new Genre(1,"Comedy",false),
            new Genre(2,"Horror",false),
            new Genre(3,"Drama",false),
        };

        public static List<Book> BookList { get; set; } = new List<Book>()
        {
            new Book(0, "Martian", 1500, Genres[0],ChaptersB1, true),
            new Book(1, "Catch-22", 1200, Genres[1],ChaptersB2, false),
            new Book(2, "It", 800, Genres[2],ChaptersB3, true),
            new Book(3, "The Great Gatsby", 1000,Genres[3], ChaptersB4,false)
        };



       
           
        

       


        
      


        
    }
}
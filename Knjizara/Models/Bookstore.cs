﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Knjizara.Models
{
    public class Bookstore
    {
        public int Id  { get; set; }
        public string Name { get; set; }
        public List<Book> BookList { get; set; }
    }
}
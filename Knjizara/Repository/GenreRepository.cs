﻿using Knjizara.Models;
using Knjizara.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Knjizara.Repository
{
    public class GenreRepository : IGenreRepository
    {
        private SqlConnection conn;
        private void Connection()
        {
            string connString = ConfigurationManager.ConnectionStrings["BookStoreDbContext"].ToString();
            conn = new SqlConnection(connString);
        }

        public IEnumerable<Genre> GetAll()
        {
            string sqlQuery = "SELECT * FROM Genre WHERE GenreDeleted=0";
                 
            DataTable dt = new DataTable();
            Connection();
                       
            using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
            {
                conn.Open();
                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                ad.Fill(dt);
            }
            
            List<Genre> Genres = new List<Genre>();
            foreach (DataRow dataRow in dt.Rows)
            {
                int id = (int)dataRow["GenreId"];
                string name = dataRow["GenreName"].ToString();

                Genres.Add(new Genre() { Id = id, Name = name });
            }
            return (Genres);
        }

        public Genre GetById(int id)
        {
            string sqlQuery = "SELECT Genre SET GenreDeleted=1 WHERE GenreId=@Genreid";
            DataTable dt = new DataTable();
            Connection();

            using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
            {
                cmd.Parameters.AddWithValue("@GenreId", id);
                conn.Open();
                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                ad.Fill(dt);
            }
            DataRow row = dt.Rows[0];
           
            Genre genre = new Genre();
            genre.Id = (int)row["GenreId"];
            genre.Name = row["GenreName"].ToString();
           
            return (genre);
        }

        public bool Create(string name)
        {
            string sqlQuery = "INSERT INTO Genre (GenreName) VALUES (@GenreName)";
            Connection();
            using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
            {
                cmd.Parameters.AddWithValue("@GenreName", name);
                conn.Open();
                if ((cmd.ExecuteNonQuery()) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void Delete(int id)
        {
            string sqlQuery = "UPDATE Genre SET GenreDeleted=1 WHERE GenreId=@Genreid";
            Connection();
            using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
            {
                cmd.Parameters.AddWithValue("@GenreId", id);
                conn.Open();
                cmd.ExecuteNonQuery();
            }
        }
    }
}
 



 
             
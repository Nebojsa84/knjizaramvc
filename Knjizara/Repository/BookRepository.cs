﻿using Knjizara.Models;
using Knjizara.Repository.Interfaces;
using Knjizara.ViewModel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Knjizara.Repository
{
    public class BookRepository : IBookRepository
    {
        private SqlConnection conn;
        private void Connection()
        {
            string connString = ConfigurationManager.ConnectionStrings["BookStoreDbContext"].ToString();
            conn = new SqlConnection(connString);
        }

        public bool Create(Book book, int genreId)
        {
            string sqlQuery = "INSERT INTO Book (BookName,BookPrice,GenreId) VALUES (@BookName,@BookPrice,@GenreId)";

            Connection();

            using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
            {
                cmd.Parameters.AddWithValue("@BookName", book.Name);
                cmd.Parameters.AddWithValue("@BookPrice", book.Price);
                cmd.Parameters.AddWithValue("@GenreId", genreId);
                conn.Open();

                if ((cmd.ExecuteNonQuery()) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public void Delete(int id)
        {
           string sqlQuery = "UPDATE Book SET BookDeleted=1 WHERE BookId=@BookId";
            Connection();       
           using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
           {
               cmd.Parameters.AddWithValue("@BookId", id);
               conn.Open();
               cmd.ExecuteNonQuery();
           }          
        }

        public IEnumerable<Book> GetAll()
        {
            string sql = "SELECT * FROM Book b INNER JOIN Genre g ON b.GenreId=g.GenreId WHERE b.BookDeleted=0";
            Connection();
            List<Book> Books = new List<Book>();
            DataTable dt = new DataTable();

            using (SqlCommand comm = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);
            }

            foreach (DataRow dr in dt.Rows)
            {
                Book book = new Book();
                book.Id = (int)dr["BookId"];
                book.Name = dr["BookName"].ToString();
                book.Price = (int)dr["BookPrice"];
                Genre genre = new Genre();
                genre.Id = (int)dr["GenreId"];
                genre.Name = dr["GenreName"].ToString();
                book.Genre = genre;
                Books.Add(book);
            }

            return (Books);
        }

        public Book GetById(int id)
        {
            string BookQuery = "SELECT * FROM Book b INNER JOIN Genre g ON b.GenreId=g.GenreId WHERE b.BookDeleted=0 AND b.BookId=@id";

            Connection();

            DataTable dt = new DataTable();

            using (SqlCommand cmd = new SqlCommand(BookQuery, conn))
            {
                conn.Open();
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                ad.Fill(dt);
            }
            DataRow row = dt.Rows[0];

            Book book = new Book();
            book.Id = (int)row["BookId"];
            book.Name = row["BookName"].ToString();
            book.Price = (int)row["BookPrice"];
            Genre genre = new Genre();
            genre.Id = (int)row["GenreId"];
            genre.Name = row["GenreName"].ToString();
            
            book.Genre = genre;

            return (book);
        }

       

        public void Update(BookGenreViewModel bookVM)
        {
            string sqlQuery = "UPDATE Book SET BookName=@BookName,BookPrice=@BookPrice,genreId=@GenreId  WHERE BookId=@BookId";
            Connection();
            using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
            {
                cmd.Parameters.AddWithValue("@GenreId", bookVM.SelectedGenre);
                cmd.Parameters.AddWithValue("@BookId", bookVM.Book.Id);
                cmd.Parameters.AddWithValue("@BookPrice", bookVM.Book.Price);
                cmd.Parameters.AddWithValue("@BookName", bookVM.Book.Name);
                conn.Open();
                cmd.ExecuteNonQuery();

            }
        }

        public IEnumerable<Book> GetDeleted()
        {
            string sql = "SELECT * FROM Book b INNER JOIN Genre g ON b.GenreId=g.GenreId WHERE b.BookDeleted=1";
            Connection();
            List<Book> Books = new List<Book>();
            DataTable dt = new DataTable();

            using (SqlCommand comm = new SqlCommand(sql, conn))
            {
                conn.Open();
                SqlDataAdapter da = new SqlDataAdapter(comm);
                da.Fill(dt);
            }

            foreach (DataRow dr in dt.Rows)
            {
                Book book = new Book();
                book.Id = (int)dr["BookId"];
                book.Name = dr["BookName"].ToString();
                book.Price = (int)dr["BookPrice"];
                Genre genre = new Genre();
                genre.Id = (int)dr["GenreId"];
                genre.Name = dr["GenreName"].ToString();
                book.Genre = genre;
                Books.Add(book);
            }

            return (Books);
        }

        public IEnumerable<Book> FindBook(string name, string criteria)
        {
            string sqlQuery = "";

            if (criteria == "name")
            {
                sqlQuery = "SELECT* FROM Book b INNER JOIN Genre g ON b.GenreId = g.GenreId WHERE b.BookDeleted = 0 AND b.BookName LIKE @name";
            }
            else if (criteria == "chapter")
            {

            }

            Connection();

            DataTable BookDt = new DataTable();
                       
            using (SqlCommand cmd = new SqlCommand(sqlQuery, conn))
            {
                cmd.Parameters.AddWithValue("@name", "%" + name + "%");
                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                ad.Fill(BookDt);
            }
            

            List<Book> Books = new List<Book>();
            foreach (DataRow dataRow in BookDt.Rows)
            {
                int id = (int)dataRow["BookId"];
                string bookName = dataRow["BookName"].ToString();
                int price = (int)dataRow["BookPrice"];
                Genre genre = new Genre();
                genre.Id = (int)dataRow["GenreId"];
                genre.Name = dataRow["GenreName"].ToString();

                Books.Add(new Book() { Id = id, Name = bookName, Price = price, Genre = genre });
            }

            return (Books);
        }
    }
}
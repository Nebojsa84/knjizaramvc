﻿using Knjizara.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjizara.Repository.Interfaces
{
    interface IGenreRepository
    {
        IEnumerable<Genre> GetAll();
        Genre GetById(int id);
        bool Create(string name);
        void Delete(int id);
    }
}

﻿using Knjizara.Models;
using Knjizara.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Knjizara.Repository.Interfaces
{
    interface IBookRepository
    {
        IEnumerable<Book> GetAll();
        IEnumerable<Book> GetDeleted();
        IEnumerable<Book> FindBook(string name,string criteria);
        Book GetById(int id);
        bool Create(Book book,int genreId);
        void Update(BookGenreViewModel bookVM);
        void Delete(int id);
    }
}

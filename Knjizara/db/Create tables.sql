use BookStore

create table Genre (
    GenreId int primary key identity (1,1),
    GenreName nvarchar(50),      
    GenreDeleted bit DEFAULT 0    
);

create TABLE Book(
    BookId int primary key identity (1,1),
    BookName nvarchar(80),
	BookPrice int,
    GenreId int,
    foreign key (GenreId) REFERENCES Genre(GenreId),
    BookDeleted bit DEFAULT 0
);
create table Chapter (
    ChapterId int primary key identity (1,1),
    ChapterName nvarchar(50),
	BookId int,
    foreign key(BookId) REFERENCES Book(BookId),
    ChapterDeleted bit DEFAULT 0
);




        
        
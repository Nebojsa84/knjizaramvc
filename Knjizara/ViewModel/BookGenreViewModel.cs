﻿using Knjizara.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Knjizara.ViewModel
{
    public class BookGenreViewModel
    {
        public List<Genre> Genres { get; set; }
        public Book Book { get; set; }
        public int SelectedGenre { get; set; }
    }
}